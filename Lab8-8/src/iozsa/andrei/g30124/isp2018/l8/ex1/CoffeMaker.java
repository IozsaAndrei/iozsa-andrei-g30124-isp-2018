package iozsa.andrei.g30124.isp2018.l8.ex1;

class CofeeMaker {
    static int numberOfCoffees=0;
    Cofee makeCofee () throws numberException{
        if(numberOfCoffees > 10 )
            throw new numberException(numberOfCoffees,"Enough coffees");

        System.out.println("Make a coffe");
        int t = (int)(Math.random()*100);
        int c = (int)(Math.random()*100);
        Cofee cofee = new Cofee(t,c);
        numberOfCoffees ++ ;
        return cofee;
    }

}
