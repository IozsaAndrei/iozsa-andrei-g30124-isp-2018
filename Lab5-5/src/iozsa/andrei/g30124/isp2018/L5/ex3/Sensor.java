package iozsa.andrei.g30124.isp2018.L5.ex3;

public abstract class Sensor {
    private String location;
    public abstract int readValue();

    public String getLocation()
    {
        return location;
    }
}
