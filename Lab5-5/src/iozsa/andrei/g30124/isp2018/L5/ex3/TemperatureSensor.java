package iozsa.andrei.g30124.isp2018.L5.ex3;

import java.util.Random;

public class TemperatureSensor extends Sensor{
    public int tempSensor;

    @Override
    public int readValue() {
        Random number=new Random();
        tempSensor=number.nextInt(100);
        return tempSensor;
    }


}

