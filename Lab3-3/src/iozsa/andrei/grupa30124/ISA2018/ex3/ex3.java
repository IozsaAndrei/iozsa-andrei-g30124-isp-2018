package iozsa.andrei.grupa30124.ISA2018.ex3;
import becker.robots.*;
public class ex3 {
	public static void main(String[] args)
	{
		City ny = new City();
		Robot Dave = new Robot(ny, 1, 1, Direction.NORTH);
		Dave.move();
		Dave.move();
		Dave.move();
		Dave.move();
		Dave.move();
		
		Dave.turnLeft();
		Dave.turnLeft();
		
		Dave.move();
		Dave.move();
		Dave.move();
		Dave.move();
		Dave.move();
	}
}
