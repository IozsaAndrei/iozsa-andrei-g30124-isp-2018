package iozsa.andrei.grupa30124.ISA2018.ex4;
import becker.robots.*;
public class ex4 {
	public static void main(String[] args)
	{
		City cj = new City();
		Robot Dave = new Robot(cj, 0, 2, Direction.WEST);
		Wall bloc0 = new Wall(cj,1,1,Direction.NORTH);
		Wall bloc1 = new Wall(cj,1,2,Direction.NORTH);
		Wall bloc2 = new Wall(cj,1,2,Direction.EAST);
		Wall bloc3 = new Wall(cj,2,2,Direction.EAST);
		Wall bloc4 = new Wall(cj,2,2,Direction.SOUTH);
		Wall bloc5 = new Wall(cj,2,1,Direction.SOUTH);
		Wall bloc6 = new Wall(cj,1,1,Direction.WEST);
		Wall bloc7 = new Wall(cj,2,1,Direction.WEST);
		Dave.move();
		Dave.move();
		Dave.turnLeft();
		Dave.move();
		Dave.move();
		Dave.move();
		Dave.turnLeft();
		Dave.move();
		Dave.move();
		Dave.move();
		Dave.turnLeft();
		Dave.move();
		Dave.move();
		Dave.move();
		Dave.turnLeft();
		Dave.move();
		
	}

}
