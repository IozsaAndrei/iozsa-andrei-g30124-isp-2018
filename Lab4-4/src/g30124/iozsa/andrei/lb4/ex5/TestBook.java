package g30124.iozsa.andrei.lb4.ex5;

import g30124.iozsa.andrei.lb4.ex4.Author;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
public class TestBook {
	
	Book book;
    Author author;
    @Before
    public void setUp()
    { author=new Author("Iozsa","iozsaandrei@gmail.com",'m');
        book=new Book("Andrei",author,0.01);
    }
    @Test
    public void testName()
    {
        assertEquals(book.getAuthor(),"Colt alb");

    }
    @Test
    public void getQty()
    {
        book.setQtyInStock(12);
        assertEquals(book.getQtyInStock(),12);

    }
    @Test
    public void shouldPrintInfo()
    {
        assertEquals("Book name=Andrei Author Iozsa (m) at iozsaandrei@gmail.com",book.toString());
    }
}


