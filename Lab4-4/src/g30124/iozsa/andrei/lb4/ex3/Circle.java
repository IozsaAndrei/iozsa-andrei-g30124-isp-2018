package g30124.iozsa.andrei.lb4.ex3;

public class Circle {
	double radius;
	String color;

	void Circle()
	{
		radius = 1;
		color = "red";
	}

	void Circle(double r,String c)
	{
		radius = r;
		color = c;
	}

	double getArea()
	{
		return 2*3.14*radius;
	}

	double getRadius()
	{
		return radius;
	}

	public static void main(String[] args)
	{
		Circle x = new Circle();
		x.Circle();
		System.out.println(x.getRadius() + "  " + x.getArea() + "  ");
		x.Circle(10,"blue");
		System.out.println(x.getRadius() + "  " + x.getArea() + "  ");
	}

	}




