package g30124.iozsa.andrei.lb4.ex4;
import org.junit.Before;
import org.junit.Test;
import static junit.framework.Assert.assertEquals;
public class TestAuthor {
	 private Author author;
	    @Before
	    public void setUp(){
	        author=new Author("Iozsa Andrei ","iozsaandrei@yahoo.com",'m');

	    }
	    @Test
	    public void setEmailTest()
	    {
	        author.setEmail("avramalexandru@gmail.com");
	        assertEquals(author.getEmail(),"avramalexandru@gmail.com");
	    }
	    @Test
	    public void getGenderTest()
	    {
	        assertEquals(author.getGender(),'m');
	    }
	    @Test
	    public void getNameTest()
	    {
	        assertEquals(author.getName(),"Iozsa Andrei");
	    }
	    @Test
	    public void stringMethod()
	    {
	        Author a3=new Author("Both","bothniculae@gmail.com",'m');
	        assertEquals(a3.toString(),"Author Both (m) at bothnicolae@gmail.com");
	    }
	}

	


