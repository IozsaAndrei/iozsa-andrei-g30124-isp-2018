package g30124.iozsa.andrei.lb4.ex7;
import static java.lang.Math.round;
public class Circle {
	
	private double radius=1.0;
	private String color="red";
	public Circle()
	{
	    radius=1.0;
	    color="red";
	}
	public Circle(double radius)
	{
	    this.radius=radius;
	}
	public double getRadius(
	)
	{
	    return this.radius;
	}
	public double getArea()
	{
	    return round((Math.PI*Math.pow(radius,2))*100.0)/100.0;
	}

	    public String toString() {
	        return "Circle radius "+ radius +
	                ", color=" + color ;
	    }
	}

