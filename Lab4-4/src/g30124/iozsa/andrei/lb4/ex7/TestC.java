package g30124.iozsa.andrei.lb4.ex7;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;

public class TestC {

	 Circle circle;
	    Cylinder cylinder;
	    Cylinder cylinder1;
	    @Before
	    public void setUp()
	    {
	        cylinder=new Cylinder(2.5,4);
	        circle=new Circle(2.5);
	        cylinder1=new Cylinder();
	    }
	    @Test
	    public void shouldGetRadius()
	    {
	        assertEquals(circle.getRadius(),2.5);
	        assertEquals(cylinder1.getRadius(),1.0); // implicit constructor
	    }
	    @Test
	    public void shouldGetArea()
	    {
	        assertEquals(circle.getArea(),19.63);
	        assertEquals(cylinder.getArea(),102.09,2);
	    }
	    @Test
	    public void shouldPrintInfos()
	    {
	        assertEquals(circle.toString(),"Circle radius 2.5, color=red");
	    }
	    @Test
	    public void shouldGetVolume()
	    {
	        assertEquals(cylinder.getVolume(),78.52);
	    }
	    @Test
	    public void shouldGetHeight()
	    {
	        assertEquals(cylinder.getHeight(),4.0);
	    }


	}

