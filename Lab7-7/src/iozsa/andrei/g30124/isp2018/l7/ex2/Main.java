package iozsa.andrei.g30124.isp2018.l7.ex2;
import iozsa.andrei.g30124.isp2018.l7.ex1.BankAccount;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;

public class Main {
    public static void main(String[] args) {
       Bank bank=new Bank();

       bank.addAccount("Andrei",200.2);
       bank.addAccount("Stefan",1201);
       bank.addAccount("Marius",40);
       bank.printAccounts();
        System.out.println("-----");
        //bank.printAccounts(20,300);

        ArrayList<BankAccount> bankAccounts= bank.getAccounts();

        Collections.sort(bankAccounts,BankAccount.BankAccountComparator);
        for(BankAccount b:bankAccounts)
            System.out.println(b.toString());

    }
}
