package iozsa.andrei.g30124.isp2018.l6.ex2;
import java.awt.*;

public  interface  Shape {
    void draw(Graphics g);
    String getId();
}
