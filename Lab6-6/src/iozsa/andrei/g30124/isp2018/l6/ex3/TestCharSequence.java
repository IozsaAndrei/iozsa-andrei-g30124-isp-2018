package iozsa.andrei.g30124.isp2018.l6.ex3;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

public class TestCharSequence {
    private CharSequence charSequence;
    @Before
    public void set()
    {  char[] new_sir;
         new_sir="da";
        charSequence=new CharSequence(new_sir);
    }
    @Test
    public void shouldGetChar()
    {
        assertEquals(charSequence.charAt(2),"o");
    }
}

